Generate a docker image based on registry.gitlab.com/pages/hugo:latest with Git installed

`registry.gitlab.com/capedev-labs/docker/hugo-with-git/master:latest` will contain the last built image from the master.
It's better to use image built from tag as the hugo version is fixed (see below).

##### Usage in `.gitlab-ci.yml`:
```yaml
build:
  image: registry.gitlab.com/capedev-labs/docker/hugo-with-git/0-55-6:latest
  stage: build
  script:
  - hugo
  artifacts: 
    paths:
    - public
```


##### Link
Documentation: https://docs.gitlab.com/ee/ci/docker/using_docker_build.html